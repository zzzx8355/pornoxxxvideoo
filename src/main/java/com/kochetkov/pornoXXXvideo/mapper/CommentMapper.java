package com.kochetkov.pornoXXXvideo.mapper;

import com.kochetkov.pornoXXXvideo.dto.commentDto.CommentCreateDto;
import com.kochetkov.pornoXXXvideo.dto.commentDto.CommentFullDto;
import com.kochetkov.pornoXXXvideo.dto.commentDto.CommentPreviewDto;
import com.kochetkov.pornoXXXvideo.dto.commentDto.CommentUpdateDto;
import com.kochetkov.pornoXXXvideo.entity.Actris;
import com.kochetkov.pornoXXXvideo.entity.Comment;

import java.util.ArrayList;
import java.util.List;

public class CommentMapper {

    public List<CommentPreviewDto> mapToDtoList(List<Comment> entities) {
        List<CommentPreviewDto> dtos = new ArrayList<>();

        for (Comment entity : entities) {
            CommentPreviewDto dto = new CommentPreviewDto();

            dto.setId(entity.getId());
            dto.setActris(entity.getActris());
            dto.setMessage(entity.getMessage());
            dto.setName(entity.getName());
            dto.setSurname(entity.getSurname());
            dto.setEmail(entity.getEmail());
            dto.setMark(entity.getMark());
            dto.setPublished(entity.isPublished());

            dtos.add(dto);
        }
        return dtos;

    }

    public Comment mapToEntity(CommentCreateDto createDto , Actris actris) {
        Comment comment = new Comment();
        comment.setMessage(createDto.getMessage());
        comment.setName(createDto.getName());
        comment.setSurname(createDto.getSurname());
        comment.setEmail(createDto.getEmail());
        comment.setMark(createDto.getMark());
        comment.setActris(actris);

        return comment;
    }

    public Comment mapToEntity(CommentUpdateDto updateDto) {
        Comment comment = new Comment();
        comment.setId(updateDto.getId());
        comment.setMessage(updateDto.getMessage());
        comment.setMark(updateDto.getMark());

        return comment;
    }

    public CommentFullDto mapToDto(Comment entity) {
        CommentFullDto fullDto = new CommentFullDto();
        fullDto.setId(entity.getId());
        fullDto.setActris(entity.getActris());
        fullDto.setMessage(entity.getMessage());
        fullDto.setName(entity.getName());
        fullDto.setSurname(entity.getSurname());
        fullDto.setEmail(entity.getEmail());
        fullDto.setMark(entity.getMark());
        fullDto.setPublished(entity.isPublished());

        return fullDto;
    }
}
