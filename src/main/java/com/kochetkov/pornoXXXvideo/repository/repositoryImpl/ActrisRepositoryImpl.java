package com.kochetkov.pornoXXXvideo.repository.repositoryImpl;

import com.kochetkov.pornoXXXvideo.entity.Actris;
import com.kochetkov.pornoXXXvideo.repository.ActrisRepository;
import com.kochetkov.pornoXXXvideo.utill.EntityManagerUtils;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

import static com.kochetkov.pornoXXXvideo.utill.EntityManagerUtils.getEntityManager;

@Repository
public class ActrisRepositoryImpl implements ActrisRepository {

    @Override
    public Actris findByEmail(String email) {
        EntityManager em = getEntityManager();

        Actris foundActris = (Actris) em.createNativeQuery(
                String.format("SELECT * FROM actris WHERE email = \"%s\"", email), Actris.class)
                .getSingleResult();

        em.close();

        System.out.println("Found: " + foundActris);
        return foundActris;
    }

    @Override
    public List<Actris> findAll() {
        EntityManager em = getEntityManager();

        List<Actris> foundList = em.createNativeQuery("SELECT * FROM `actris`", Actris.class)
                .getResultList();

        em.close();
        System.out.println("Found " + foundList.size() + " actris");
        return foundList;
    }

    @Override
    public Actris findById(Long id) {
        EntityManager em = getEntityManager();

        Actris foundActris = em.find(Actris.class, id);

        if (foundActris != null) {
            Hibernate.initialize(foundActris.getComments());
        }
        em.close();
        return foundActris;
    }

    @Override
    public Actris create(Actris actris) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        em.persist(actris);

        em.getTransaction().commit();
        em.close();
        System.out.println("Actris was created. Id: " + actris.getId());
        return actris;
    }

    @Override
    public Actris update(Actris actris) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        em.merge(actris);

        em.getTransaction().commit();
        em.close();
        System.out.println("Actris was updated. Id: " + actris.getId());
        return actris;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        Actris foundActris = em.find(Actris.class, id);
        em.remove(foundActris);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {
        EntityManager entityManager = EntityManagerUtils.getEntityManager();
        entityManager.getTransaction().begin();

        entityManager.createNativeQuery("DELETE FROM actris ").executeUpdate();

        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
